package com.zuitt.example;
import java.util.Scanner;

public class UsersInput {
    public static void main (String[] args){
        Scanner myObj = new Scanner(System.in);

        System.out.println("Enter username: ");

        String userName = myObj.nextLine();

        System.out.printf("Username is %s%n", userName);

        System.out.println("Enter a number to add:");
        System.out.print("Enter first number: ");
        int num1 = myObj.nextInt();
        System.out.print("Enter second number: ");
        int num2 = myObj.nextInt();

        System.out.printf("The sum of two numbers is %d", num1+num2);


    }
}