package com.zuitt.example;

// package - used to group related classes
    // packages are divided into two categories
        //1. Built - in packages (Packages from Java API)
        //2. User - defined packages (created by user)
// Package creation in Java follows the "reverse domain name notation" for the naming convention.
public class Variables {
    public static void main(String[] args){
        // Naming Convention
            //the terminology used for variable names is identifier.
            // All identifiers should begin with a letter, currency or an underscore
            //After the first character, identifiers can have any combination of characters.
            //Most importantly, identifiers are case-sensitive
        // variable declaration
        int age;
        char middleName;
        int x;

        // Variable declaration with initialization
        int y = 0;

        // initialization after declaration;
        x = 1;

        //output to system
        System.out.println("The value of y is " + y + " and the value of x is " + x);

        // Primitive Data types
            //predefined within the Java Programming language which is used for single-values variables with limited capabilities

        // int - whole number
        int wholeNumber = 100;
        System.out.println(wholeNumber);

        // long - L is added to be recognized as long
        long longNumber = 100000000000L;
        System.out.println(longNumber);

        // Floating value
        // float - F is added to be recognized as float
        // up to 7 decimal places
        float floatNumber = 3.14159265359F;
        System.out.println(floatNumber);

        // double - larger float; no more letters
        double doubleNumber = 3.14159265359;
        System.out.println(doubleNumber);

        //char - single letter; uses ''
        char letter = 'e';
        System.out.println(letter);

        // boolean
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        // constants
        // Final
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        // Non-primitive Data
            // reference data types - instances or objects
            // does not directly store the value of a variable, but rather remember the reference to that variable
        // String
        // stores an array of characters
        // can use methods

        String username = "JSmith";
        System.out.println(username);
        int stringLength = username.length();
        System.out.println(stringLength);



    }
}